package com.glearning.springbootdemo;

import com.glearning.springbootdemo.model.Employee;
import com.glearning.springbootdemo.service.EmployeeService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.util.Set;

@RestController
@RequestMapping("/api/employees")
@AllArgsConstructor
@OpenAPIDefinition(
        info = @Info(
         title = "employees management application",
         contact = @Contact(
              name="pradeep",
              email = "dev@glearning.com",
              url = "glearning.com"
         )
        )
)
public class EmployeeRestController {

    private EmployeeService employeeService;

    @GetMapping

    @ApiResponse(
         responseCode = "200",
         description = "to fetch all the employess"
    )
    public Set<Employee> fetchAllEmployees(@RequestParam(value = "name",  required = false) String name){
        return this.employeeService.fetchAllEmployees(name);
    }

    @GetMapping("/{empId}")
    public Employee fetchEmployeeById(@PathVariable long empId){
        return this.employeeService.findEmployeeById(empId);
    }

    @PostMapping
    public Employee saveEmployee(@RequestBody Employee employee){
        return  this.employeeService.save(employee);
    }

    @DeleteMapping("/{empid}")
    public void deleteEmployeeById(@PathVariable("empid") long id){
        this.employeeService.deleteEmployeeById(id);
    }

    @PutMapping("/{empId}")
    public Employee updateEmployee(@PathVariable long empId, @RequestBody Employee employee){
        return this.employeeService.updateEmployee(empId, employee);
    }
}
