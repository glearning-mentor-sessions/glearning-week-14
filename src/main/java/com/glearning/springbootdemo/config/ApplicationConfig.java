package com.glearning.springbootdemo.config;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Configuration;

@Configuration
class DBConfig implements HealthIndicator {

    @Override
    public Health health() {
        return Health.up().withDetail("DB", "DB is up").build();
    }
}
@Configuration
class KafkaConfig implements HealthIndicator {

    @Override
    public Health health() {
        return Health.up().withDetail("Kafka", "Kafka service up").build();
    }
}
@Configuration
class PaymentGateway implements HealthIndicator {

    @Override
    public Health health() {
        return Health.up().withDetail("Payment-Gateway", "Payment gateway service is up").build();
    }
}
