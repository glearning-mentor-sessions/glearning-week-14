package com.glearning.springbootdemo.service;

import com.glearning.springbootdemo.model.Employee;
import org.springframework.stereotype.Service;

import java.lang.invoke.LambdaConversionException;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private static final Set<Employee> employees = new HashSet<>();

    public Employee save(Employee employee){
        Employee employee1 = new Employee(12, "Ramesh", "ramesh@gmail.com", LocalDate.now());
        // new Employee(11, 33, "suresh", "vinod", 12, 33);
        Employee employee2  = Employee.builder().dob(LocalDate.now()).email("rameh@gmail.com").empId(23).name("ramesh").build();
        employees.add(employee);
        return  employee;
    }

    public Set<Employee> fetchAllEmployees(String name){
      return  name == null ? Set.copyOf(employees): employees
                                                        .stream()
                                                        .filter(employee -> employee.getName().equalsIgnoreCase(name))
                                                        .collect(Collectors.toSet());
    }

    public Employee findEmployeeById(long empId){
        /*Optional<Employee> optional = employees.stream().filter(employee -> employee.getEmpId() == empId).findFirst();
        if (optional.isPresent()){
            return optional.get();
        } else {
            throw new IllegalArgumentException("Employee with the given empid does not exists");
        }*/

        return employees
                    .stream()
                    .filter(employee -> employee.getEmpId() == empId)
                    .findFirst()
                    .orElseThrow(() ->  new IllegalArgumentException("Employee with the given empid does not exists"));
    }

    public void deleteEmployeeById(long empId){
        employees.removeIf(employee -> employee.getEmpId() == empId);
    }

    public Employee updateEmployee(long empId, Employee employee){
        Employee updatedEmployee = employees
                .stream()
                .filter(emp -> employee.getEmpId() == empId)
                .findFirst()
                .map(returnedEmployee -> {
                    returnedEmployee.setName(employee.getName());
                    returnedEmployee.setDob(employee.getDob());
                    returnedEmployee.setEmail(employee.getEmail());
                    return returnedEmployee;
                })
                .orElseThrow(() -> new IllegalArgumentException("Invalid empid passed"));

        employees.removeIf(e -> e.getEmpId() == empId);
        employees.add(updatedEmployee);
        return updatedEmployee;
    }
}
