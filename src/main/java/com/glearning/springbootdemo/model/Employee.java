package com.glearning.springbootdemo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@Builder
public class Employee  {
    private long empId;
    private String name;
    private String email;
    private LocalDate dob;
}
